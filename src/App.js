import React, { Component } from 'react';
import Contactos from './components/Contactos';

class App extends Component {
  render() {
    return (
      <Contactos />
    );
  }
}

export default App;
