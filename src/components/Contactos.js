import React, { Component } from 'react';
import axios from 'axios';

import Buscador from './Buscador';

class Contactos extends Component {
  constructor(){
    super();

    this.state = {
      datos: [],
      'id': 0
    }

    axios.get('http://localhost:3001/api/users')
    .then(response => {
      this.setState({
        datos: response.data
      })
      console.log(this.state.datos);
    })
    .catch(error => {
      console.log(error);
    })
  }

  //Funcion para eliminar usuario
  eliminarContacto(id){
    //console.log(id);
    axios.delete(`http://localhost:3001/api/users/`+id)
    .then(res => {
      console.log(res);
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    return (
      <div className="container">
        <Buscador/>
        <table className="table bg-light">
          <thead>
            <tr>
              <th className="name">Nombre</th>
              <th className="descripcion">Descripción</th>
            </tr>
          </thead>
          <tbody id="contenedor-usuarios">
            {this.state.datos.map(data =>
              <tr key={data.name} id={data.id}>
                <td className="name">
                  <img className="img_user" src={data.photo} alt=""/>
                  <h6>{data.name}</h6>
                  <p className="btn-eliminar" onClick={this.eliminarContacto.bind(this, data.id)}>Eliminar</p>
                </td>
                <td className="descripcion text">
                  {data.description}
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Contactos;