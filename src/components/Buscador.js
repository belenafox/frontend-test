import React, { Component } from 'react';
import axios from 'axios';

class Buscador extends Component {
    constructor(){
        super();
    
        this.state = {
          id: '',
          photo: '',
          name: '',
          description: ''          
        }
    
        
      }

      addUser(){
        axios.post(`http://localhost:3001/api/users/`, {
          "id": this.state.id,
          "name": this.state.name,
          "description": this.state.description,
          "photo": this.state.photo
      });

        
      }

      render(){
        return(
          <div>
            {/*modal*/}
            <div className="modal fade" id="nuevoContacto" tabIndex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="modalLabel">Agregar Nuevo Contacto</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form action="" onSubmit={this.addUser.bind(this)}>
                    <div className="form-group">
                      <label htmlFor="url-contacto">URL Imágen de Perfil<span className="asterisco">*</span></label>
                      <input type="text" className="form-control" id="url-contacto" value={this.state.photo} onChange={this.photoChange.bind(this)}/>
                    </div>
                    <div className="form-group">
                      <label htmlFor="nombre-contacto">Nombre<span className="asterisco">*</span></label>
                      <input type="text" className="form-control" id="nombre-contacto" value={this.state.name} onChange={this.nameChange.bind(this)}/>
                    </div>
                    <div className="form-group">
                      <label htmlFor="descripcion-contacto">Descripción<span className="asterisco">*</span></label>
                      <textarea className="form-control" id="descripcion-contacto" rows="3" value={this.state.description} onChange={this.descChange.bind(this)}></textarea>
                    </div>

                    <button className="btn btn-guardar" type="submit">Guardar</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            {/*buscador y botón para agregar contacto*/}
            <div className="container contenedor-buscador">
              <div className="row">
                <i id="icon-search" className="fas fa-search"></i>
                <input id="buscador" className="form-control col-md-3" type="search" placeholder="Search" aria-label="Search"/>
                <button className="btn col-md-3 ml-auto new-contact" data-toggle="modal" data-target="#nuevoContacto"><i className="fas fa-plus-circle"></i>Nuevo Contacto</button>
              </div>
            </div>
          </div>
        );
      }

      photoChange(e) {
        this.setState({ 
          photo: e.target.value
        });
      }
      nameChange(e) {
        this.setState({ 
          name: e.target.value
        });
      }
      descChange(e) {
        this.setState({ 
          description: e.target.value
        });
      }
}

export default Buscador;